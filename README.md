patriot-theme
===
WordPress theme for the [Untamed Politics][up].

## Installation

Install from sources.

## License

UNLICENSED. Copyright (c) 2016-17 Untamed Politics. All rights reserved.

[up]: https://blog.untamedpolitics.com

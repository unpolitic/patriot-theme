<form role="search" method="get" class="search-form" action="https://untamedpolitics.com/">
	<span class="screen-reader-text">Search for:</span>
	<input type="search" class="search-field" placeholder="Search …" value="" name="s">
	<input type="submit" class="search-submit" value="">
</form>

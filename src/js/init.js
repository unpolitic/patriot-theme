( function( $ ) {

  $(document).ready( function() {
      $('.tab a').click(function(e){
        e.preventDefault();
        var elem = $(e.currentTarget);
        elem.parent().parent().find('.tab').removeClass('active');
        elem.parent().addClass('active');
        var par = elem.closest('.choose2');
        par.find('.c2').removeClass('active');
        par.find(elem.attr('href')).addClass('active');
      });
  });

}( jQuery ));

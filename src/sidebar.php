<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Patriot
 */

if ( (is_home() && !is_active_sidebar( 'sidebar-1' )) ||
     (!is_home() && !is_active_sidebar( 'sidebar-2' )) )
	return;
?>

<aside id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-' . (is_home() ? '1' : '2') ); ?>
</aside><!-- #secondary -->

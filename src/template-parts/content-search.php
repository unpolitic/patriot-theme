<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Patriot
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(is_single() ? null : 'post-summary'); ?>>
  <?php
    $style = '';
    if ( !is_single() ) {
    $temp = get_the_post_thumbnail_url( $post->ID, 'full' );
    $style = "background-image: url('$temp');";
    $style = 'style="' . $style . '"';
  } ?>
	<header class="entry-header" <?php echo $style; ?>>
    <div id="title-and-meta">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php patriot_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

    </div>
    <?php
    if (is_single()) {
      the_post_thumbnail( 'full' );
    }
    ?>

	</header><!-- .entry-header -->

  <?php
    if (is_singular()) { ?>
  <div class="entry-content">
    <?php } ?>

    <?php
      if (is_singular()) {
  			the_content( sprintf(
  				/* translators: %s: Name of current post. */
  				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'patriot' ), array( 'span' => array( 'class' => array() ) ) ),
  				the_title( '<span class="screen-reader-text">"', '"</span>', false )
  			) );

  			wp_link_pages( array(
  				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'patriot' ),
  				'after'  => '</div>',
  			) );

      }
		?>

    <?php
      if (is_singular()) { ?>
    </div>
      <?php } ?>

  <?php if (is_singular()) : ?>
	<footer class="entry-footer">
		<?php patriot_entry_footer(); ?>
	</footer><!-- .entry-footer -->
  <?php endif; ?>
</article><!-- #post-## -->
